FROM gitlab-registry.cern.ch/industrial-controls/sw-infra/jenkins/master-master:2.277

USER root

RUN /usr/local/bin/install-plugins.sh docker-plugin:1.2.2 openstack-cloud:2.58

USER jenkins

ENTRYPOINT ["/usr/bin/dumb-init", "--"]
CMD ["/usr/libexec/run"]
